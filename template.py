# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool

__all__ = ['Template']


class Template(metaclass=PoolMeta):
    __name__ = 'electronic.mail.template'

    queue = fields.Boolean('Queue',
        help='Put these messages in the output mailbox instead of sending '
            'them immediately.')
    mailbox_outbox = fields.Many2One('electronic.mail.mailbox',
        'Outbox Mailbox', help='Mailbox outbox to send mail',
        states={'required': Bool(Eval('queue'))},
        depends=['queue'])

    @classmethod
    def send_mail(cls, mail_id, template=False):
        pool = Pool()
        ElectronicMail = Pool().get('electronic.mail')
        EmailConfiguration = pool.get('electronic.mail.configuration')
        email_configuration = EmailConfiguration(1)

        if template and template.queue:
            mailbox = (template.mailbox_outbox
                if template.mailbox_outbox else email_configuration.outbox)
            mail = ElectronicMail(mail_id)
            mail.mailbox = mailbox.id
            mail.save()
            return

        # if not queue or it is called from cron sending email
        super(Template, cls).send_mail(mail_id, template=template)
