# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool

__all__ = ['Mailbox', 'ElectronicMail']


class Mailbox(metaclass=PoolMeta):
    __name__ = 'electronic.mail.mailbox'

    scheduler = fields.Boolean('Scheduler',
        help='Send emails in this mailbox by the scheduler')

    @staticmethod
    def default_scheduler():
        return False


class ElectronicMail(metaclass=PoolMeta):
    __name__ = 'electronic.mail'

    @classmethod
    def send_mails_scheduler(cls, args=None):
        pool = Pool()
        Mailbox = pool.get('electronic.mail.mailbox')
        EMailConfiguration = pool.get('electronic.mail.configuration')
        email_configuration = EMailConfiguration(1)
        sent_mailbox = email_configuration.sent

        limit = None
        if args:
            try:
                limit = int(args)
            except (TypeError, ValueError):
                pass

        mailboxes = Mailbox.search([
            ('scheduler', '=', True),
            ])
        if not mailboxes:
            return

        emails = cls.search([
            ('mailbox', 'in', mailboxes),
            ('flag_send', '=', False)
            ], order=[('date', 'ASC')], limit=limit)

        cls.send_mail([e for e in emails if e.from_ and e.to])

        emails = cls.browse([e.id for e in emails])
        for email in emails:
            if email.flag_send:
                email.mailbox = sent_mailbox
        cls.save(emails)
