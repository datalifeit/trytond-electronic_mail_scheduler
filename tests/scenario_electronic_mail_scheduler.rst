==================================
Electronic Mail Scheduler Scenario
==================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install electronic_mail_scheduler::

    >>> config = activate_modules('electronic_mail_scheduler')
