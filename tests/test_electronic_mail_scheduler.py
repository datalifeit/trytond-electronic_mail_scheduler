# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
import doctest
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
import trytond.tests.test_tryton
from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_checker
from trytond.pool import Pool

FROM = 'tryton@example.com'
TO = 'user@example.com'


class ElectronicMailSchedulerTestCase(ModuleTestCase):
    """Test Electronic Mail Scheduler module"""
    module = 'electronic_mail_scheduler'

    def _create_template(self, model, outbox):
        pool = Pool()
        Template = pool.get('electronic.mail.template')
        SMTPServer = pool.get('smtp.server')
        Mailbox = pool.get('electronic.mail.mailbox')

        mailbox, = Mailbox.create([{
                    'name': 'Parent Mailbox',
                    }])

        smtp_server, = SMTPServer.create([{
                'name': 'TestSMTP',
                'smtp_server': 'smtp.com',
                'smtp_email': TO,
                'state': 'done'

        }])

        template, = Template.create([{
            'name': 'template',
            'from_': FROM,
            'to': TO,
            'smtp_server': smtp_server.id,
            'mailbox': mailbox.id,
            'draft_mailbox': mailbox.id,
            'mailbox_outbox': outbox.id,
            'model': model.id,
            'queue': True
        }])
        return template

    @with_transaction()
    def test_create_email_queue_template(self):
        "Test email is created on trigger and not sent"
        pool = Pool()
        Model = pool.get('ir.model')
        User = pool.get('res.user')
        Trigger = pool.get('ir.trigger')
        Model = pool.get('ir.model')
        Mailbox = pool.get('electronic.mail.mailbox')
        ElectronicMail = pool.get('electronic.mail')
        ElectronicMailTemplate = pool.get('electronic.mail.template')

        outbox_mailbox, = Mailbox.create([{
                    'name': 'Parent Mailbox',
                    }])

        model, = Model.search([
                ('model', '=', User.__name__),
                ])
        template = self._create_template(model, outbox_mailbox)

        trigger, = Trigger.create([{
                    'name': 'Electronic mail creation',
                    'model': model.id,
                    'on_create': True,
                    'condition': 'true',
                    'action': 'electronic.mail.template|mail_from_trigger',
                    'email_template': template.id
                    }])
        users = User.create([{'name': "Michael Scott", 'login': "msc"}])
        # trigger is queued so mail is not rendered until method ends
        # we render it manually
        ElectronicMailTemplate.mail_from_trigger(users, trigger)
        email, = ElectronicMail.search([])
        self.assertEqual(email.flag_send, 0)
        self.assertEqual(email.mailbox, outbox_mailbox)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        ElectronicMailSchedulerTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_electronic_mail_scheduler.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
