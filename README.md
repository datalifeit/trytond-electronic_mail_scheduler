datalife_electronic_mail_scheduler
==================================

The electronic_mail_scheduler module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-electronic_mail_scheduler/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-electronic_mail_scheduler)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
